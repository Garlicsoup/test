package Tutorial.Testing;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Annotations {

	@BeforeTest
	public void Init()
	{
		System.out.println("Init Test");
	}
	
	@Test(dataProvider="data-provider")
	public void Test1(String userName, String password)
	{
		System.out.println("U: "+userName+"P: "+password);
	}
	
	@Test(timeOut = 2)
	public void Test2()
	{
		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("test 2");
	}
	
	@DataProvider(name = "data-provider")
	public Object[][] getData()
	{
		//i nr times to run
		//j nr of parmaters to send for 1 go
		Object[][] data = new Object[3][2];
		
		data[0][0] = "userName";
		data[0][1] = "passWord";
		data[1][0] = "randy";
		data[1][1] = "hak";
		data[2][0] = "bla";
		data[2][1] = "dibla";
		
		return data;
	}
	
}
